<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Player extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Player_model', 'model', TRUE);
    }

    function index()
    {
        $this->load->helper('form');
        $data['titulo'] = "Lord of the Shirtz | Cadastro de Personagens";
        $data['players'] = $this->model->listar();
        $this->load->view('player_view.php', $data);
    }

    function inserir() {

        /* Carrega a biblioteca do CodeIgniter responsável pela validação dos formulários */
        $this->load->library('form_validation');

        /* Define as tags onde a mensagem de erro será exibida na página */
        $this->form_validation->set_error_delimiters('<span>', '</span>');

        /* Define as regras para validação */
        $this->form_validation->set_rules('name', 'Nome', 'required|max_length[40]');
        $this->form_validation->set_rules('lifePoints', 'Pontos de Vida', 'trim|required');
        $this->form_validation->set_rules('strength', 'Força', 'trim|required');
        $this->form_validation->set_rules('agility', 'Agilidade', 'trim|required');

        /* Executa a validação e caso houver erro chama a função index do controlador */
        if ($this->form_validation->run() === FALSE) {
            $this->index();
            /* Senão, caso sucesso: */
        } else {
            /* Recebe os dados do formulário (visão) */
            $data['name'] = $this->input->post('name');
            $data['lifePoints'] = $this->input->post('lifePoints');
            $data['strength'] = $this->input->post('strength');
            $data['agility'] = $this->input->post('agility');

            /* Carrega o modelo */
            $this->load->model('Player_model', 'model', TRUE);

            /* Chama a função inserir do modelo */
            if ($this->model->inserir($data)) {
                redirect('admin/player');
            } else {
                log_message('error', 'Erro ao inserir o personagem.');
            }
        }
    }

    function editar($id)  {

        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Lord of the Shirtz | Edição de Personagens";

        /* Busca os dados da pessoa que será editada */
        $data['player'] = $this->model->editar($id);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('player_edit', $data);
    }

    function atualizar() {

        /* Carrega a biblioteca do CodeIgniter responsável pela validação dos formulários */
        $this->load->library('form_validation');

        /* Define as tags onde a mensagem de erro será exibida na página */
        $this->form_validation->set_error_delimiters('<span>', '</span>');

        /* Aqui estou definindo as regras de validação do formulário, assim como
           na função inserir do controlador, porém estou mudando a forma de escrita */
        $this->form_validation->set_rules('name', 'Nome', 'required|max_length[40]');
        $this->form_validation->set_rules('lifePoints', 'Pontos de Vida', 'trim|required');
        $this->form_validation->set_rules('strength', 'Força', 'trim|required');
        $this->form_validation->set_rules('agility', 'Agilidade', 'trim|required');

        /* Executa a validação e caso houver erro chama a função editar do controlador novamente */
        if ($this->form_validation->run() === FALSE) {
            $this->editar($this->input->post('id'));
        } else {
            /* Senão obtém os dados do formulário */
            $data['id'] = $this->input->post('id');
            $data['name'] = ucwords($this->input->post('name'));
            $data['lifePoints'] = $this->input->post('lifePoints');
            $data['strength'] = $this->input->post('strength');
            $data['agility'] = $this->input->post('agility');

            /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
            if ($this->model->atualizar($data)) {
                redirect('admin/player');
            } else {
                log_message('error', 'Erro ao atualizar o personagem.');
            }
        }
    }

    function deletar($id) {

        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id)) {
            redirect('admin/player');
        } else {
            log_message('error', 'Erro ao deletar o personagem.');
        }
    }
}