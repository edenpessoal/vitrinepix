<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class DiceApi extends REST_Controller {

    public function roll_get()
    {
        $valor = rand(1, $this->get('faces'));
        $this->response(array("valor", $valor));
    }

}