<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class PlayerApi extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function players_get()
    {
        $this->load->model('Player_model');
        $data['players'] = $this->Player_model->listar();

        $this->response($data['players']);

    }

}