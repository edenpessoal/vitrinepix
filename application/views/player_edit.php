<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title><?php echo $titulo; ?></title>
    <meta charset="UTF-8" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/estilo.css"/>
</head>
<body>
<?php echo form_open('admin/player/atualizar', 'id="form-player"'); ?>

<input type="hidden" name="id" value="<?php echo $player[0]->id; ?>"/>

<label for="name">Nome:</label><br/>
<input type="text" name="name" value="<?php echo $player[0]->name; ?>"/>
<div class="error"><?php echo form_error('name'); ?></div>

<label for="lifePoints">Pontos de vida:</label><br/>
<input type="text" name="lifePoints" value="<?php echo $player[0]->lifePoints; ?>"/>
<div class="error"><?php echo form_error('lifePoints'); ?></div>

<label for="strength">Força:</label><br/>
<input type="text" name="strength" value="<?php echo $player[0]->strength; ?>"/>
<div class="error"><?php echo form_error('strength'); ?></div>

<label for="agility">Agilidade:</label><br/>
<input type="text" name="agility" value="<?php echo $player[0]->agility; ?>"/>
<div class="error"><?php echo form_error('agility'); ?></div>

<input type="submit" name="atualizar" value="Atualizar" />

<?php echo form_close(); ?>
</body>
</html>