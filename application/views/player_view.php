<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <title><?php echo $titulo; ?></title>
    <meta charset="UTF-8" />
</head>
<body>
<?php echo form_open('admin/player/inserir', 'id="form-player"'); ?>

<label for="name">Nome:</label><br/>
<input type="text" name="name" value="<?php echo set_value('name'); ?>" required/>
<div class="error"><?php echo form_error('name'); ?></div>

<label for="lifePoints">Pontos de vida:</label><br/>
<input type="number" name="lifePoints" value="<?php echo set_value('lifePoints'); ?>" required />
<div class="error"><?php echo form_error('lifePoints'); ?></div>

<label for="strength">Força:</label><br/>
<input type="number" name="strength" value="<?php echo set_value('strength'); ?>" required />
<div class="error"><?php echo form_error('strength'); ?></div>

<label for="agility">Agilidade:</label><br/>
<input type="number" name="agility" value="<?php echo set_value('agility'); ?>" required />
<div class="error"><?php echo form_error('agility'); ?></div>


<input type="submit" name="cadastrar" value="Cadastrar" />

<?php echo form_close(); ?>



<!-- Lista as Personaagens Cadastradas -->
<div id="grid-pessoas">
    <ul>
        <?php foreach($players as $player): ?>
            <li>
                <a title="Deletar" href="<?php echo base_url() . 'admin/player/deletar/' . $player->id; ?>" onclick="return confirm('Confirma a exclusão deste personagem?')">X</a>
                <span> - </span>
                <a title="Editar" href="<?php echo base_url() . 'admin/player/editar/' . $player->id; ?>"><?php echo $player->name; ?></a>
                <span> - </span>
                <span><?php echo $player->lifePoints; ?></span>
                <span> - </span>
                <span><?php echo $player->strength; ?></span>
                <span> - </span>
                <span><?php echo $player->agility; ?></span>
            </li>
        <?php endforeach ?>
    </ul>
</div>
<!-- Fim Lista -->

</body>
</html>