<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>VitrinePix - RPG</title>

	<!-- Jquery (CDN) -->
	<script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<script src="<?=base_url()?>assets/js/main.js"></script>
	<script src="<?=base_url()?>assets/js/gameplay.js"></script>
	<script src="<?=base_url()?>assets/js/player.js"></script>
	<script src="<?=base_url()?>assets/js/weapon.js"></script>
	<script src="<?=base_url()?>assets/js/dice.js"></script>


	<style type="text/css">
		.blocoexterno{
			padding: 20px;
			border: 1px solid #000;
			border-radius: 5px;
			margin-top: 20px;
			background: #f6f6f6;
		}
		.interna1{
			margin-top: 20px;
			border-radius: 5px;
			background: rgba(102, 102, 102, 0.18);
			padding: 21px 11px;
		}
		.ogro{ width: 100%;  height: 423px;}
		p { margin: 0 0 3px 0;}
		.mt20{ margin-top: 20px;}
		.mb20{margin-bottom: 20px;}
		.fundo{background: url(https://cronicasdeumdm.files.wordpress.com/2013/04/04_cave.png); background-size: cover;  padding-bottom: 40px;}

		.acaotela{background: #fff; margin-top: 20px;}
		.acaobotoes{background: none; margin-top: 20px; text-align: center;}

		.acaotitulo h1{text-align: center; color: #000;}

		.acaotitulo { background: #fff;  border-radius: 0px 0px 30px 30px;}

		img {
			max-width: 100%;
			height: auto;
			border: 0;
		}

		.pdv{
			position: absolute;
			font-size: 44px;
			color: red;
			font-family: Helvetica,Arial,sans-serif;
		}
	</style>

</head>
<body>

<div class="fundo">


	<div class="container mt20 mb20">
		<div class="col-md-4 blocoexterno">

			<div id="player1_pdv" class="pdv">20</div>

			<img class="ogro" src="http://static.omelete.uol.com.br/media/filer_public/73/3b/733bf9b7-930f-4ad8-b02c-5897f20821b2/warcraft-orc.jpg">

			<div class="row">
				<div class="col-md-6 mt20">
					<p>Força: +1</p>
					<p>Agilidade: +2</p>
				</div>
				<div class="col-md-6 interna1">
					<img src="http://s3.ktkbr.com.br/wp-content/blogs.dir/11/files/2011/11/doomhammer_kotakubr.jpg">
					<label>Espada Longa</label>
					<p>Ataque: <label>+2</label></p>
					<p>Defesa: <label>+1</label></p>
					<p>Dano: <label>1d6</label></p>
				</div>
			</div>

		</div>


		<div class="col-md-4">

			<div class="col-md-12 acaotitulo">
				<h1>Lord of the Shirtz</h1>
			</div>

			<div class="col-md-12 acaotela">

				<div class="col-md-12">
					<!-- Acao guerreiro 1 -->
					<div class="col-md-6">
						HUMANO
					</div>
					<!-- Acao guerreiro 2 -->
					<div class="col-md-6">
						ORC
					</div>

				</div>

			</div>


			<div class="col-md-12 acaobotoes">
				<button type="button" id="iniciar-iniciativa" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Iniciar Batalha</button>
			</div>

			<div class="col-md-12">
				<button type="button" id="player1-atacar" class="btn btn-info btn-lg pull-left hide" >Atacar!</button>
				<button type="button" id="player2-atacar" class="btn btn-info btn-lg pull-right hide" >Atacar!</button>
			</div>

			<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Iniciativa</h4>
						</div>

						<div class="modal-body">

							<p class="col-md-12" id="title-iniciativa">Rolem os dados para determinar quem inicia o ataque</p>

							<div class="col-md-12">

								<div class="col-md-6">
									<label>HUMANO</label>
								</div>

								<div class="col-md-6">
									<label>ORC</label>
								</div>

							</div>

							<div class="col-md-12">

								<div class="col-md-6">
									<input type="button" id="iniciativa-player1" class="btn btn-warning" value="Rolar!" />
								</div>

								<div class="col-md-6">
									<input type="button" id="iniciativa-player2" class="btn btn-warning" value="Rolar!" />
								</div>

							</div>

							<div class="col-md-12">

								<div class="col-md-6">
									<p id="valor-iniciativa-player1"></p>
								</div>

								<div class="col-md-6">
									<p id="valor-iniciativa-player2"></p>
								</div>

							</div>

						</div>

						<div class="modal-footer">
							<button type="button" id="iniciativa-prosseguir" class="btn btn-default" data-dismiss="modal" disabled>Prosseguir >></button>
						</div>
					</div>

				</div>
			</div>

		</div>


		<div class="col-md-4 blocoexterno">

			<div id="player2_pdv" class="pdv">20</div>

			<img class="ogro"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQInaB3y1rNB3LoD7_gZLJr13MER2S8gkSAyNobHztiJ_RVGNky">

			<div class="row">
				<div class="col-md-6 mt20">
					<p>Força: +2</p>
					<p>Agilidade: 0</p>
				</div>
				<div class="col-md-6 interna1">
					<img src="http://s3.ktkbr.com.br/wp-content/blogs.dir/11/files/2011/11/doomhammer_kotakubr.jpg">
					<label>Clava de Madeira</label>
					<p>Ataque: <label>+1</label></p>
					<p>Defesa: <label>0</label></p>
					<p>Dano: <label>1d8</label></p>
				</div>
			</div>

		</div>

	</div>
</div>




</body>
</html>