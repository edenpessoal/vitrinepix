var iniciativa = [];
var playerTurno = null;

//function rollDice(faces, callback){
//    $.get("/dice/roll/faces/" +faces, callback);
//}
//
//function treatInitiative(player, valor){
//
//    iniciativa[player] = valor;
//
//    if(iniciativa.length >= 2){
//
//        var nomeJogador;
//
//        // ambos os jogadores ja rolaram os dados
//        // TODO: somar a agilidade do personagem
//        if(iniciativa[0] > iniciativa[1]){
//            playerTurno = 0;
//            // TODO: dados estarão no banco posteriormente
//            nomeJogador = "Humano";
//        }else{
//            // TODO: considerar o empate
//            playerTurno = 1;
//            nomeJogador = "Orc";
//        }
//
//        $("#title-iniciativa")
//            .html("O JOGADOR " + nomeJogador + " VENCEU A DISPUTA!!");
//
//        $("#iniciativa-prosseguir").prop("disabled",false);
//        $("#iniciar-iniciativa").hide();
//        $("#player1-atacar, #player2-atacar").removeClass("hide");
//
//    }
//}
//
//function attack(){
//
//}

$(function() {

    $("#iniciativa-player1").click( function(e){

        $(this).prop("disabled",true);

        var callback = function(retorno, status){
            $("#valor-iniciativa-player1").html( retorno[1] );
            treatInitiative(0, retorno[1]);
        };

        rollDice(20, callback);
    });

    $("#iniciativa-player2").click( function(e){

        $(this).prop("disabled",true);

        var callback = function(retorno, status){
            $("#valor-iniciativa-player2").html( retorno[1] );
            treatInitiative(1, retorno[1]);
        };

        rollDice(20, callback);
    });
});